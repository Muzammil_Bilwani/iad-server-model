var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var config = require('../config/config');
var log = require('tracer').console({ format: "{{message}}  - {{file}}:{{line}}" }).log;
var db = require('../models/index')

/* GET users listing. */
router.get('/', function (req, res, next) {
log("user");
db.user.findAll({}).then(
  function(response){
    res.send(response);
  },
  function(err){

    log(err)
    res.statusCode = 500;
      var resBody = {
        error : err.errors,
        suucess: false,
        message : err.message,
      }
      res.send(resBody);
  }
)

});

router.post('/', function (req, res, next) {
  log(req.body);
  var factory = {
    name: req.body.name,
    email: req.body.revenue || '' 
  }
  db.user.create(factory).then(function (response) {

    log(response);
    res.send(response);

  },
    function (err) {
      log(err.errors);
      res.statusCode = 500;
      var resBody = {
        error : err.errors,
        suucess: false,
        message : err.message,
      }
      res.send(resBody);
    })
})


router.get('/:id',function(req,res,next){

  db.user.findOne({where : {id : req.params.id}}).then(
    function(response){
       log(response);
    res.send(response);
    },
    function (err) {
      log(err.errors);
      res.statusCode = 500;
      var resBody = {
        error : err.errors,
        suucess: false,
        message : err.message,
      }
      res.send(resBody);
    })
  

})




module.exports = router;
